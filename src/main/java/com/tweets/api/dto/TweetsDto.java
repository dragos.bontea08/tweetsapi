package com.tweets.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.ZonedDateTime;

public class TweetsDto implements Comparable<TweetsDto>{
    @JsonProperty("userName")
    private String userName;
    @JsonProperty("tweet")
    private String tweet;
    @JsonProperty("createdAt")
    private String createdAt;

    public TweetsDto(String userName, String tweet, String createdAt) {
        this.userName = userName;
        this.tweet = tweet;
        this.createdAt = createdAt;
    }

    public String getUserName() {
        return userName;
    }

    public String getTweet() {
        return tweet;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    @Override
    public int compareTo(TweetsDto o) {
        if(ZonedDateTime.parse(this.createdAt).isBefore(ZonedDateTime.parse(o.createdAt))) {
            return -1;
        }
        return 1;
    }
}
