package com.tweets.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserTweetDto {
    @JsonProperty
    private String tweet;

    public String getTweet() {
        return tweet;
    }
}
