package com.tweets.api.controller;

import com.tweets.api.dto.TweetsDto;
import com.tweets.api.dto.UserTweetDto;
import com.tweets.api.exception.TweetTooLongException;
import com.tweets.api.service.UserTweetsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
public class UserTweetsController {
    @Autowired
    UserTweetsService userTweetsService;

    @GetMapping(value = "/tweets/mine", produces = "application/json")
    public List<TweetsDto> getTweets(Principal principal) {
        return userTweetsService.getCurrentUserTweets(principal.getName());
    }

    @GetMapping(value = "/tweets", produces = "application/json")
    public List<TweetsDto> getOtherUsersTweets(Principal principal) {
        return userTweetsService.getOtherUsersTweets(principal.getName());
    }

    @PostMapping(value = "/tweets", consumes = "application/json")
    public void addTweet(Principal principal, @RequestBody UserTweetDto tweet) throws TweetTooLongException {
        userTweetsService.addTweet(principal.getName(), tweet.getTweet());
    }
}
