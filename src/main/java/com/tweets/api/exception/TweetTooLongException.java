package com.tweets.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.server.ResponseStatusException;

@ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY, reason = "Tweet too long")
public class TweetTooLongException extends Exception {

    public TweetTooLongException() {
        super();
    }
}
