package com.tweets.api.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.tweets.api.dto.TweetsDto;
import com.tweets.api.model.Tweet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Repository
public class UserTweetsDao {

    private final DynamoDBMapper mapper;

    @Autowired
    public UserTweetsDao(DynamoDBMapper mapper) {
        this.mapper = mapper;
    }

    public Tweet saveTweet(String userName, String tweet, String createdAt) {
        Tweet t = new Tweet();
        t.setTweet(tweet);
        t.setUserName(userName);
        t.setCreatedAt(createdAt);
        mapper.save(t);
        return t;
    }

    public List<TweetsDto> getUserTweets(String userName) {
        return getTweets(userName, "userName = :name" );
    }

    public List<TweetsDto> getOtherUserTweets(String userName) {
        return getTweets(userName, "userName <> :name" );
    }

    private List<TweetsDto> getTweets(String userName, String filterExpression) {
        HashMap<String, AttributeValue> eav = new HashMap<>();
        eav.put(":name", new AttributeValue().withS(userName));

        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
                .withFilterExpression(filterExpression)
                .withExpressionAttributeValues(eav);

        List<TweetsDto> tweetList = new ArrayList<>();
        List<Tweet> scan = mapper.scan(Tweet.class, scanExpression);
        for(Tweet el : scan) {
            tweetList.add(new TweetsDto(el.getUserName(), el.getTweet(), el.getCreatedAt()));
        }
        return tweetList;
    }
}
