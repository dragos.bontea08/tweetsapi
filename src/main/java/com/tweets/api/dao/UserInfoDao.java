package com.tweets.api.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.tweets.api.model.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserInfoDao {

    private final DynamoDBMapper mapper;

    @Autowired
    public UserInfoDao(DynamoDBMapper mapper) {
        this.mapper = mapper;
    }

    public void saveUser(UserInfo userInfo) {
        mapper.save(userInfo);
    }

    public UserInfo getUser(String userName) {
       return mapper.load(UserInfo.class, userName);
    }
}
