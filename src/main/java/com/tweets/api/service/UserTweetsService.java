package com.tweets.api.service;

import com.tweets.api.dao.UserTweetsDao;
import com.tweets.api.dto.TweetsDto;
import com.tweets.api.exception.TweetTooLongException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class UserTweetsService {

    private final UserTweetsDao userTweetsDao;

    @Autowired
    public UserTweetsService(UserTweetsDao userTweetsDao) {
        this.userTweetsDao = userTweetsDao;
    }

    private static final int TWEET_CHAR_MAX = 160;

    public void addTweet(String userName, String tweet) throws TweetTooLongException {
        if (tweet.length() > TWEET_CHAR_MAX) {
            throw new TweetTooLongException();
        }

        String createdAt = ZonedDateTime.now().toOffsetDateTime().toString();
        userTweetsDao.saveTweet(userName, tweet, createdAt);
    }

    public List<TweetsDto> getCurrentUserTweets(String userName) {
        List<TweetsDto> userTweets = userTweetsDao.getUserTweets(userName);
        if (userTweets == null) {
            return new ArrayList<>();
        }
        Collections.sort(userTweets);
        return userTweets;
    }

    public List<TweetsDto> getOtherUsersTweets(String userName) {
        List<TweetsDto> otherUserTweets = userTweetsDao.getOtherUserTweets(userName);
        if (otherUserTweets == null) {
            return new ArrayList<>();
        }
        if (otherUserTweets.size() > 1) {
            List<TweetsDto> asd = new ArrayList<>(otherUserTweets);
            Collections.sort(asd);
            return asd;
        }
        return otherUserTweets;
    }
}
