package com.tweets.api.service;

import com.tweets.api.dao.UserInfoDao;
import com.tweets.api.model.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailService implements UserDetailsService {

    private final UserInfoDao userInfoDao;
    @Autowired
    public CustomUserDetailService(UserInfoDao userInfoDao) {
        this.userInfoDao = userInfoDao;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final UserInfo user = userInfoDao.getUser(username);

        if(user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return User.withUsername(user.getUserName())
                .password(user.getUserPassword())
                .roles("USER")
                .build();
    }
}
