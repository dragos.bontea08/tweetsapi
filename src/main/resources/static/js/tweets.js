function getMyTweets() {
    fetch('/tweets/mine')
    .then(data => data.json())
    .then(result => {
        var arrayLength = result.length;
        document.getElementById('my-tweets').innerHTML = '';
        for (var i = 0; i < arrayLength; i++) {
            let listOfTweets = document.getElementById('my-tweets');
            listOfTweets.innerHTML += "<li>" + "Tweet: " + result[i].tweet + "</li>";
            listOfTweets.innerHTML += "<li class=>" + "at: " + result[i].createdAt + "</li>";
        }
    });
}

function getCsrfToken() {
    return document.querySelector('meta[name="_csrf"]').content;
}

async function postTweet() {
    const tweetInput = document.getElementById('tweet-input-id').value;
    if(tweetInput === null) {
        return;
    }
    fetch("/tweets", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({tweet: tweetInput}),
    })
    .then(checkError)
    .then(jsonResponse => {
    }).catch((error) => {
        alert(error);
    });
}

function checkError(response) {
  if (response.status >= 200 && response.status <= 299) {
    return response;
  } else {
    return response.text().then(text => {
    throw new Error(text);
    });
  }
}

function getAllTweets() {
    fetch('/tweets')
        .then(data => data.json())
        .then(tweets => {
            document.getElementById('all-tweets').innerHTML = '';
            for (var i = 0; i < tweets.length; i++) {
                document.getElementById('all-tweets').innerHTML += '<li>'
                + tweets[i].userName + ' tweeted: ' + tweets[i].tweet+ '</li>' +
                'at: ' + tweets[i].createdAt + '</li>';
            }
        });
}

