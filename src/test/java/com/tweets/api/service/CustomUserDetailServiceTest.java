package com.tweets.api.service;

import com.tweets.api.dao.UserInfoDao;
import com.tweets.api.model.UserInfo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CustomUserDetailServiceTest {

    @Mock
    private UserInfoDao userInfoDao;
    @Mock
    private UserInfo userInfo;

    private CustomUserDetailService userDetailsService;

    @BeforeEach
    void init(){
        userDetailsService = new CustomUserDetailService(userInfoDao);
    }

    @Test
    public void shouldLoadUserByUserName() {
        // given
        String userName = "fake-user";
        String password = "fake-password";
        when(userInfoDao.getUser(userName)).thenReturn(userInfo);
        when(userInfo.getUserName()).thenReturn(userName);
        when(userInfo.getUserPassword()).thenReturn(password);

        // when
        UserDetails userDetails = userDetailsService.loadUserByUsername(userName);

        // then
        assertThat(userDetails.getUsername()).isEqualTo(userName);
        assertThat(userDetails.getPassword()).isEqualTo(password);
    }

    @Test
    public void shouldNotLoadWhenUserNameIsNull() {
        // given
        String userName = "fake-user";
        when(userInfoDao.getUser(userName)).thenReturn(null);

        // then
        assertThatThrownBy(() -> userDetailsService.loadUserByUsername(userName))
                .isInstanceOf(UsernameNotFoundException.class);

    }

}
