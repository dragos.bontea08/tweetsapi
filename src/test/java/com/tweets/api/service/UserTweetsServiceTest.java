package com.tweets.api.service;

import com.tweets.api.dao.UserTweetsDao;
import com.tweets.api.dto.TweetsDto;
import com.tweets.api.exception.TweetTooLongException;
import com.tweets.api.model.Tweet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserTweetsServiceTest {

    private static final ZonedDateTime DATE1 =
            ZonedDateTime.of(
                    LocalDate.of(2021, 10, 01),
                    LocalTime.of(12, 24, 10),
                    ZoneId.of("UTC"));
    private static final ZonedDateTime DATE2 =
            ZonedDateTime.of(
                    LocalDate.of(2021, 10, 01),
                    LocalTime.of(12, 24, 9),
                    ZoneId.of("UTC"));

    @Mock
    private UserTweetsDao userTweetsDao;
    @Mock
    private Tweet tweet;

    private UserTweetsService userTweetsService;


    @BeforeEach
    public void init() {
        userTweetsService = new UserTweetsService(userTweetsDao);
    }

    @Test
    public void shouldAddTweet() throws TweetTooLongException {
        // given
        String userName = "fake-user";
        String tweet = "fake-tweet";
        Tweet tweets = mock(Tweet.class);
        when(userTweetsDao.saveTweet(eq(userName), eq(tweet), any())).thenReturn(tweets);

        // when
        userTweetsService.addTweet(userName, tweet);
    }

    @Test
    public void shouldThrowExceptionWhenInputTooLarge() {
        // given
        String userName = "fake-user";
        String tweet = generateRandomString(161);

        // then
        assertThatThrownBy(() -> userTweetsService.addTweet(userName, tweet))
                .isInstanceOf(TweetTooLongException.class);

    }

    @Test
    public void shouldGetCurrentUsersTweets() {
        // given
        String userName = "fake-user";
        String tweet = "fake-tweet";
        String createdAt = "fake-createdAt";
        TweetsDto tweetsDto = new TweetsDto(userName, tweet, createdAt);

        when(userTweetsDao.getUserTweets(userName)).thenReturn(Collections.singletonList(tweetsDto));

        // when
        List<TweetsDto> currentUserTweets = userTweetsService.getCurrentUserTweets(userName);

        // then
        assertThat(currentUserTweets)
                .extracting(TweetsDto::getTweet)
                .anyMatch(value -> value.matches(tweet));
    }

    @Test
    public void shouldGetOtherUsersTweets() {
        // given
        String userName = "fake-user";
        String tweet = "fake-tweet";
        String createdAt = ZonedDateTime.now().toOffsetDateTime().toString();
        TweetsDto tweetsDto = new TweetsDto(userName, tweet, createdAt);
        List<TweetsDto> tweetsDto1 = List.of(tweetsDto);

        when(userTweetsDao.getOtherUserTweets(userName)).thenReturn(List.of(tweetsDto));

        // when
        List<TweetsDto> otherUsersTweets = userTweetsService.getOtherUsersTweets(userName);

        // then
        assertThat(otherUsersTweets)
                .extracting(TweetsDto::getTweet)
                .anyMatch(value -> value.matches(tweet));
    }

    @Test
    public void shouldSortOtherTweets() {
        // given
        String userName = "fake-user";
        String tweet = "fake-tweet";
        String createdAt1 = DATE1.toOffsetDateTime().toString();
        String createdAt2 = DATE2.toOffsetDateTime().toString();
        TweetsDto tweetsDto = new TweetsDto(userName, tweet, createdAt1);
        TweetsDto tweetsDto1 = new TweetsDto(userName, tweet, createdAt2);


        when(userTweetsDao.getOtherUserTweets(userName)).thenReturn(List.of(tweetsDto, tweetsDto1));

        // when
        List<TweetsDto> otherUsersTweets = userTweetsService.getOtherUsersTweets(userName);

        // then
        assertThat(otherUsersTweets.size()).isEqualTo(2);
        otherUsersTweets.get(0);
        assertThat(otherUsersTweets.get(0).getCreatedAt()).isEqualTo(createdAt2);
        otherUsersTweets.get(1);
        assertThat(otherUsersTweets.get(1).getCreatedAt()).isEqualTo(createdAt1);
    }

    private String generateRandomString(int length) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .limit(length)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }
}
