package com.tweets.api.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.tweets.api.model.UserInfo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserInfoDaoTest {
    @Mock
    private DynamoDBMapper mapper;
    @Mock
    UserInfo userInfo;

    private UserInfoDao userInfoDao;

    @BeforeEach
    public void init() {
        userInfoDao = new UserInfoDao(mapper);
    }

    @Test
    public void shouldSaveUser() {
        // given
        doNothing().when(mapper).save(userInfo);

        // when
        userInfoDao.saveUser(userInfo);

        // then
        verify(mapper, times(1)).save(userInfo);
    }

    @Test
    public void shouldGetUser() {
        // given
        String userName = "fake-user";
        when(mapper.load(UserInfo.class, userName))
                .thenReturn(userInfo);

        // when
        UserInfo actualUserInfo = userInfoDao.getUser(userName);

        // then
        assertThat(actualUserInfo).isEqualTo(userInfo);
    }

}
