package com.tweets.api.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import com.tweets.api.dto.TweetsDto;
import com.tweets.api.model.Tweet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.internal.stubbing.defaultanswers.ForwardsInvocations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserTweetsDaoTest {

    private final DynamoDBMapper mapper = mock(DynamoDBMapper.class);

    private static final ZonedDateTime DATE1 =
            ZonedDateTime.of(
                    LocalDate.of(2021, 10, 01),
                    LocalTime.of(12, 24, 10),
                    ZoneId.of("UTC"));
    private static final ZonedDateTime DATE2 =
            ZonedDateTime.of(
                    LocalDate.of(2021, 10, 01),
                    LocalTime.of(12, 24, 9),
                    ZoneId.of("UTC"));

    private UserTweetsDao userTweetsDao;

    @BeforeEach
    public void init() {
        userTweetsDao = new UserTweetsDao(mapper);
    }

    private Tweet createMockedTweet(String userName, String tweetString, String createdAt) {
        Tweet tweet = new Tweet();
        tweet.setUserName(userName);
        tweet.setTweet(tweetString);
        tweet.setCreatedAt(createdAt);

        return tweet;
    }

    @Test
    public void shouldSaveTweet() {
        TweetsDto tweet = new TweetsDto("fake-user", "fake-tweet",
                DATE1.toOffsetDateTime().toString());

        Tweet received = userTweetsDao.saveTweet(tweet.getUserName(), tweet.getTweet(),
                tweet.getUserName());

        assertThat(received.getTweet().equals(tweet.getTweet()));
        assertThat(received.getCreatedAt().equals(tweet.getCreatedAt()));
        assertThat(received.getUserName().equals(tweet.getUserName()));
        verify(mapper).save(received);
    }

    @Test
    public void shouldGetTweets() {
        // given
        String userName = "user1";

        Tweet tweet1 = createMockedTweet("user1", "random-tweet1", DATE2.toOffsetDateTime().toString());
        Tweet tweet2 = createMockedTweet("user1", "random-tweet3", DATE1.toOffsetDateTime().toString());
        List<Tweet> expectedTweets = Arrays.asList(tweet1, tweet2);

        when(mapper.scan(eq(Tweet.class), any(DynamoDBScanExpression.class)))
                .thenReturn(mock(PaginatedScanList.class, new ForwardsInvocations(expectedTweets)));

        // when
        List<TweetsDto> actualUserTweets = userTweetsDao.getUserTweets(userName);

        // then
        assertThat(actualUserTweets.size()).isEqualTo(2);
        assertThat(actualUserTweets.get(0).getUserName()).isEqualTo(tweet1.getUserName());
        assertThat(actualUserTweets.get(0).getTweet()).isEqualTo(tweet1.getTweet());
        assertThat(actualUserTweets.get(0).getCreatedAt()).isEqualTo(tweet1.getCreatedAt());
        assertThat(actualUserTweets.get(1).getUserName()).isEqualTo(tweet2.getUserName());
        assertThat(actualUserTweets.get(1).getTweet()).isEqualTo(tweet2.getTweet());
        assertThat(actualUserTweets.get(1).getCreatedAt()).isEqualTo(tweet2.getCreatedAt());
        verify(mapper)
                .scan(eq(Tweet.class), any(DynamoDBScanExpression.class));
    }

    @Test
    public void shouldGetOtherUsersTweets() {
        // given
        String userName = "user1";

        Tweet tweet2 = createMockedTweet("user2", "random-tweet2", DATE1.toOffsetDateTime().toString());
        List<Tweet> expectedTweets = Arrays.asList(tweet2);

        when(mapper.scan(eq(Tweet.class), any(DynamoDBScanExpression.class)))
                .thenReturn(mock(PaginatedScanList.class, new ForwardsInvocations(expectedTweets)));

        // when
        List<TweetsDto> actualUserTweets = userTweetsDao.getUserTweets(userName);

        // then
        assertThat(actualUserTweets.size()).isEqualTo(1);
        assertThat(actualUserTweets.get(0).getUserName()).isEqualTo(tweet2.getUserName());
        assertThat(actualUserTweets.get(0).getTweet()).isEqualTo(tweet2.getTweet());
        assertThat(actualUserTweets.get(0).getCreatedAt()).isEqualTo(tweet2.getCreatedAt());
        verify(mapper)
                .scan(eq(Tweet.class), any(DynamoDBScanExpression.class));
    }
}
