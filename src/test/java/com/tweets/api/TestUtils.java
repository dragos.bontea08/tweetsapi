package com.tweets.api;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;

public final class TestUtils {

    private TestUtils() {
    }

    public static String readResource(String path) {
        try(InputStream is = ClassLoader.getSystemResourceAsStream(path)) {
            return IOUtils.toString(is);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
