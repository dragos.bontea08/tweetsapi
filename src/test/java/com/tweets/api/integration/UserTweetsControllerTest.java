package com.tweets.api.integration;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedScanList;
import com.tweets.api.config.TestApplicationInitializer;
import com.tweets.api.model.Tweet;
import org.junit.jupiter.api.Test;
import org.mockito.internal.stubbing.defaultanswers.ForwardsInvocations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Collections;

import static com.tweets.api.TestUtils.readResource;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class UserTweetsControllerTest extends TestApplicationInitializer {

    @Autowired
    private DynamoDBMapper dynamoDBMapper;

    private static final ZonedDateTime DATE1 =
            ZonedDateTime.of(
                    LocalDate.of(2021, 10, 01),
                    LocalTime.of(12, 24, 10),
                    ZoneId.of("UTC"));
    private static final ZonedDateTime DATE2 =
            ZonedDateTime.of(
                    LocalDate.of(2021, 10, 01),
                    LocalTime.of(12, 24, 9),
                    ZoneId.of("UTC"));

    @Test
    public void shouldGetTweets() {
        // given
        Tweet tweet = createMockedTweet(USERNAME, "any-tweet", DATE1.toOffsetDateTime().toString());
        when(dynamoDBMapper.scan(eq(Tweet.class), any(DynamoDBScanExpression.class)))
                .thenReturn(mock(PaginatedScanList.class, new ForwardsInvocations(Collections.singletonList(tweet))));

        // when
        ResponseEntity<String> response = sendRequest("/tweets/mine", HttpMethod.GET, String.class);

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody()).isEqualTo(readResource("json/myTweetsResponse.json"));
    }

    //
    @Test
    public void shouldGetOtherUserTweets() {
        // given
        Tweet tweet1 = createMockedTweet("user1", "random-tweet1",
                DATE2.toOffsetDateTime().toString());
        Tweet tweet2 = createMockedTweet("user2", "random-tweet2",
                DATE1.toOffsetDateTime().toString());
        when(dynamoDBMapper.scan(eq(Tweet.class), any(DynamoDBScanExpression.class)))
                .thenReturn(mock(PaginatedScanList.class, new ForwardsInvocations(Arrays.asList(tweet1, tweet2))));

        // when
        ResponseEntity<String> response = sendRequest("/tweets", HttpMethod.GET, String.class);

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody()).isEqualTo(readResource("json/allTweetsResponse.json"));
    }

    @Test
    public void shouldSaveTweet() {
        // given
        doNothing().when(dynamoDBMapper).save(any());
        String myTweet = readResource("json/saveTweetRequest.json");

        // when
        ResponseEntity<String> response = sendRequest("/tweets", myTweet, HttpMethod.POST, String.class);

        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }


    @Test
    public void shouldThrowExceptionWhenTweetTooLarge() {
        // given
        String myTweet = readResource("json/saveTweetTooLargeRequest.json");

        // then
        assertThatThrownBy(() -> sendRequest("/tweets", myTweet, HttpMethod.POST, String.class))
                .isInstanceOf(HttpClientErrorException.class)
                .hasMessageContaining("Tweet too long");
    }


    private Tweet createMockedTweet(String userName, String tweetString, String createdAt) {
        Tweet tweet = new Tweet();
        tweet.setUserName(userName);
        tweet.setTweet(tweetString);
        tweet.setCreatedAt(createdAt);

        return tweet;
    }
}
