package com.tweets.api.config;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;


@TestConfiguration
public class TestApplicationConfig {

    @Bean
    @Primary
    public UserDetailsService userDetailsService() {
        UserDetails basicUser = User
                .withUsername("username")
                .password("password")
                .roles("USER")
                .build();

        return new InMemoryUserDetailsManager(List.of(
                basicUser
        ));
    }

    @Bean
    @Primary
    public DaoAuthenticationProvider authProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService());
        authProvider.setPasswordEncoder(NoOpPasswordEncoder.getInstance());
        return authProvider;
    }

    @MockBean
    DynamoDBMapper dynamoDBMapper;

    @Bean
    @Primary
    public DynamoDBMapper dynamoDBMapper() {
        return dynamoDBMapper;
    }
}
