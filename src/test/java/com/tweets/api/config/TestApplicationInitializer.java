package com.tweets.api.config;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ContextConfiguration(classes = TestApplicationConfig.class)
public class TestApplicationInitializer {

    private static final String BASE_PATH = "http://localhost:";
    protected static final String USERNAME = "username";
    protected static final String PASSWORD = "password";

    @LocalServerPort
    int randomServerPort;

    private RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate;
    }

    public <T> ResponseEntity<T> sendRequest(String url, HttpMethod method, Class<T> responseType) {
        return sendRequest(url, null, method, responseType);

    }
    public <R, T> ResponseEntity<T> sendRequest(String url, R body, HttpMethod method, Class<T> responseType) {
        HttpHeaders headersAndCookies = prepareHeaders();
        headersAndCookies.add("Content-Type", "application/json");
        HttpEntity request = new HttpEntity<>(body, headersAndCookies);
        return restTemplate().exchange(
                BASE_PATH + randomServerPort + url,
                method,
                request,
                responseType);
    }

    private List<HttpCookie> processHeaders(HttpHeaders headers) {
        final List<String> cooks = headers.get("Set-Cookie");
        final List<HttpCookie> cookies = new ArrayList<>();
        if (cooks != null && !cooks.isEmpty()) {
            cooks.stream().map(HttpCookie::parse).forEachOrdered((cook) -> {
                cook.forEach((a) -> {
                    cookies.stream()
                            .filter(x -> a.getName().equals(x.getName()))
                            .findAny().ifPresent(cookies::remove);
                    cookies.add(a);
                });
            });
        }
        return cookies;
    }

    private HttpHeaders prepareHeaders() {
        HttpHeaders headers = new HttpHeaders();
        ResponseEntity<String> loggedinResponse = authenticateUser(USERNAME, PASSWORD);
        List<HttpCookie> httpCookies = processHeaders(loggedinResponse.getHeaders());
        StringBuilder b = new StringBuilder();
        httpCookies.forEach(cookie -> b.append(cookie).append(";"));
        b.deleteCharAt(b.length() - 1);
        headers.add("Cookie", b.toString());
        return headers;
    }


    public ResponseEntity<String> authenticateUser(String userName, String password) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("userName", userName);
        map.add("password", password);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, null);

        return restTemplate().exchange(
                BASE_PATH + randomServerPort + "/login",
                HttpMethod.POST,
                request,
                String.class);

    }
}
